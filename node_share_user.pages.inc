<?php //$Id$

/**
 * @file
 *   Contains page callbacks for node_share_user module
 */

function node_share_user_page($node) {
  return drupal_get_form('node_share_user_page_form', $node);
}

/**
 * Form constructor for the node share page
 *
 */
function node_share_user_page_form($form_state = array(), $node) {
  $form['nid'] = array(
    '#type' => 'value',
    '#value' => $node->nid,
  );
  $form['share_title'] = array(
    '#type' => 'item',
    '#value' => '<h3>' . t('Share "@node_title" to users', array('@node_title' => $node->title)) . '</h3>',
  );
  $form['users'] = array(
    '#type' => 'textfield',
    '#title' => t('Users'),
    '#description' => t('Enter the usernames separated by commas.'),
    '#autocomplete_path' => 'node_share_user/autocomplete',
    '#required'  => TRUE,
  );
  $form['message'] = array(
    '#type' => 'textarea',
    '#title' > t('Message'),
    '#description' => t('Enter the message that you want to send to the users.'),
    '#cols' => 45,
  );
  $form['#ajax'] = array(
    'enabled' => TRUE
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Share'),
  );
  return $form;
}
  
function node_share_user_page_form_submit($form, &$form_state) {
  global $user;
  $usernames = drupal_explode_tags($form_state['values']['users']);
  $node = node_load($form_state['values']['nid']);
  $index = 0;
  $node_link = l($node->title, 'node/' . $node->nid);
  $global_username = theme('username', $user);
  foreach ($usernames as $username) {
    $account = user_load(array('name' => $username));
    //Send an email to the user.
    drupal_mail('node_share_user', 'node_share_user', $account->mail, user_preferred_language($account), 
                array('node' => $node, 'destination_account' => $account, 'source_account' => $user, 'message' => $form_state['values']['message']));
    $index++;
    watchdog('node_share_user', "The user !user shared the content !content to the user !other_user with this message: !message",
            array('!user' => $global_username, '!content' => $node_link, '!other_user' => theme('username', $account), '!message' => check_plain($form_state['values']['message'])));
  }
  drupal_set_message(t('!number messages has been sent to your selecte user(s).', array('!number' => $index)));
}

/**
 * Helper function for autocompletion. Copied from taxonomy.pages.inc
 */
function node_share_user_autocomplete($string = '') {
  // The user enters a comma-separated list of names. We only autocomplete the last name.
  $array = drupal_explode_tags($string);

  // Fetch last tag
  $last_string = trim(array_pop($array));
  $matches = array();
  if ($last_string != '') {
    $result = db_query_range(db_rewrite_sql("SELECT u.uid, u.name FROM {users} u WHERE u.status = 1 AND LOWER(u.name) LIKE LOWER('%%%s%%')", 'u', 'uid'), $last_string, 0, 10);

    $prefix = count($array) ? implode(', ', $array) .', ' : '';

    while ($account = db_fetch_object($result)) {
      $n = $account->name;
      // Commas and quotes in user names are special cases, so encode 'em.
      if (strpos($account->name, ',') !== FALSE || strpos($account->name, '"') !== FALSE) {
        $n = '"' .  str_replace('"', '""', $account->name) .'"';
      }
      $matches[$prefix . $n] = check_plain($account->name);
    }
  }

  drupal_json($matches);
}