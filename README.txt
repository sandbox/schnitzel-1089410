
Note Share to Users
=======================================


DESCRIPTION
-----------
This Module allows a User to share a node to another user.

There are a lot of other modules, which already provide this functionality. But no one of them can share to other users of the same side.
Especially if you only know the username of another user and not the Mail Address, this module will help users to share nodes.


REQUIREMENTS
------------
Drupal 6.x


INSTALLING
----------
1. To install the module copy the 'node_share_to_user' folder to your sites/all/modules directory.

2. Go to admin/build/modules. Enable the module.


CONFIGURING AND USING
---------------------

1. Got to the Content Type settings for which you want to enable this feature.

2. In the Node Links there will be a new link with "Share to user".